#pragma once
#include<string>
#include<memory>

class CompositeComponent {
public:
	virtual ~CompositeComponent() = default;
	virtual std::string Operation() const = 0;
};

class ConcreteComponent : public CompositeComponent {
public:
	virtual std::string Operation() const override {
		return "ConcreteComponent";
	}
};

class Decorator : public CompositeComponent {
protected:
	std::shared_ptr<CompositeComponent> component;
public:
	Decorator(std::shared_ptr<CompositeComponent> component) : component(component) {}

	virtual std::string Operation() const override {
		return component->Operation();
	}
};

class ConcreteDecoratorA : public Decorator {
public:
	ConcreteDecoratorA(std::shared_ptr<CompositeComponent> component) : Decorator(component) {}

	virtual std::string Operation() const override {
		return "ConcreteDecoratorA(" + Decorator::Operation() + ")";
	}
};

class ConcreteDecoratorB : public Decorator {
public:
	ConcreteDecoratorB(std::shared_ptr<CompositeComponent> component) : Decorator(component) {}

	virtual std::string Operation() const override {
		return "ConcreteDecoratorB(" + Decorator::Operation() + ")";
	}
};