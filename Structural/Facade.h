#pragma once
#include<string>
#include<memory>
class Subsystem1 {
public:
	std::string Operation1() const {
		return "Subsystem1: Ready!\n";
	}

	std::string OperationN() const {
		return "Subsystem1: Go!\n";
	}
};

class Subsystem2 {
public:
	std::string Operation1() const {
		return "Subsystem2: Get ready!\n";
	}

	std::string OperationZ() const {
		return "Subsystem2: Fire!\n";
	}
};

class Facade {
protected:
	std::shared_ptr<Subsystem1> subsystem1;
	std::shared_ptr<Subsystem2> subsystem2;

public:
	Facade(std::shared_ptr<Subsystem1> subsystem1, std::shared_ptr<Subsystem2> subsystem2) :
		subsystem1(subsystem1),
		subsystem2(subsystem2) {}

	std::string Operation() {
		std::string result = "Facade initializes subsystems:\n";
		result += this->subsystem1->Operation1();
		result += this->subsystem2->Operation1();
		result += "Facade orders subsystems to perform the action:\n";
		result += this->subsystem1->OperationN();
		result += this->subsystem2->OperationZ();
		return result;
	}
};