#pragma once
#include<memory>
class Implementation {
public:
	virtual ~Implementation() = default;
	virtual std::string OperationImplementation() const = 0;
};

class ConcreteImplementationA : public Implementation {
public:
	virtual std::string OperationImplementation() const override {
		return "ConcreteImplementationA: Here's the result on the platform A.\n";
	}
};

class ConcreteImplementationB : public Implementation {
public:
	virtual std::string OperationImplementation() const override {
		return "ConcreteImplementationB: Here's the result on the platform B.\n";
	}
};

class Abstraction {
protected:
	Implementation& implementation;

public:
	Abstraction(Implementation& implementation) : implementation(implementation) {}

	virtual ~Abstraction() = default;

	virtual std::string Operation() const {
		return "Abstraction: Base operation with:\n" +
			implementation.OperationImplementation();
	}
};

class ExtendedAbstraction : public Abstraction {
public:
	ExtendedAbstraction(Implementation& implementation) : Abstraction(implementation) {}
	virtual std::string Operation() const override {
		return "ExtendedAbstraction: Extended operation with:\n" +
			implementation.OperationImplementation();
	}
};