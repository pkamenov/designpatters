#pragma once
#include<string>
#include<memory>
class Subject {
public:
	virtual ~Subject() = default;
	virtual std::string Request() const = 0;
};

class RealSubject : public Subject {
public:
	virtual std::string Request() const override {
		return "RealSubject: Handling request.\n";
	}
};

class Proxy : public Subject {
private:
	std::shared_ptr<RealSubject> real_subject;

	bool CheckAccess() const {
		return true;
	}

	std::string LogAccess() const {
		return "Proxy: Logging the time of request.\n";
	}

public:
	Proxy(std::shared_ptr<RealSubject> real_subject) : real_subject(real_subject) {}

	virtual std::string Request() const override {
		if (CheckAccess()) {
			real_subject->Request();
			return LogAccess();
		}
	}
};