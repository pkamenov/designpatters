#pragma once
#include<memory>
#include<list>
#include<string>
class Component {
protected:
	std::shared_ptr<Component> parent;
public:
	virtual ~Component() = default;
	void SetParent(Component* newParent) {
		parent = std::shared_ptr<Component>(newParent);
	}

	const std::shared_ptr<Component> GetParent() const {
		return parent;
	}
	virtual void Add(Component* component) = 0;
	virtual void Remove(Component* component) = 0;

	virtual bool IsComposite() const {
		return false;
	}

	virtual std::string Operation() const = 0;
};

class Leaf : public Component {
public:
	std::string Operation() const override {
		return "Leaf";
	}

	virtual void Add(Component* component) override {}
	virtual void Remove(Component* component) override {}
};

class Composite : public Component {
protected:
	std::list<std::shared_ptr<Component>> children;

public:
	virtual void Add(Component* component) override {
		auto newPtr = std::shared_ptr<Component>(component);
		children.push_back(newPtr);
		newPtr->SetParent(this);
	}

	virtual void Remove(Component* component) override {
		auto newPtr = std::shared_ptr<Component>(component);
		children.remove(newPtr);
		component->SetParent(nullptr);
	}

	virtual bool IsComposite() const override {
		return true;
	}

	virtual std::string Operation() const override {
		std::string result;
		for (const auto c : children) {
			if (c == children.back()) {
				result += c->Operation();
			}
			else {
				result += c->Operation() + "+";
			}
		}
		return "Branch(" + result + ")";
	}
};