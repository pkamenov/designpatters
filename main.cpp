﻿#include<iostream>
#include<array>
#include"Factory.h"
#include"AbstractFactory.h"
#include"Builder.h"
#include"Prototype.h"
#include"Singleton.h"
#include"Adapter.h"
#include"Bridge.h"
#include"Composite.h"
#include"Decorator.h"
#include"Facade.h"
#include"Proxy.h"
#include"ChainOfResponsibility.h"
#include"Command.h"
#include"Mediator.h"
#include"Memento.h"
#include"Observer.h"
#include"State.h"
#include"Strategy.h"
#include"Template.h"
#include"Visitor.h"

int main() {
	std::cout << "------------------------------------------------------\n\nFactory:\n";
	//Factory Example
	std::shared_ptr<Creator> creator = std::make_shared<ConcreteCreator1>();
	std::cout << "Client: " << (*creator)() << std::endl;
	std::shared_ptr<Product> product = creator->FactoryMethod();
	std::cout << (*product)() << std::endl;
	//Abstract factory example
	std::cout << "------------------------------------------------------\n\nAbstractFactory:\n";
	std::shared_ptr<AbstractFactory> abstractFactory1 = std::make_shared<ConcreteFactory1>();
	std::shared_ptr<AbstractFactory> abstractFactory2 = std::make_shared<ConcreteFactory2>();
	std::shared_ptr<AbstractProductA> product1 = abstractFactory1->createProductA();
	std::shared_ptr<AbstractProductB> product2 = abstractFactory2->createProductB();
	std::cout << (*product1)() << ' ' << (*product2)() << std::endl;
	//Builder example
	std::cout << "------------------------------------------------------\n\nBuilder:\n";
	std::shared_ptr<ConcreteBuilder1> builder = std::make_shared<ConcreteBuilder1>();
	std::shared_ptr<Product1> builderProduct;
	Director director(*builder.get());
	std::cout << "Using director\n";
	director.BuildFullFeaturedProduct();
	std::cout << "Result- ";
	builderProduct = builder->getProduct();
	builderProduct->listParts();
	std::cout << "Custom build\n";
	builder->Reset();
	builder->ProducePartC();
	builder->ProducePartA();
	std::cout << "Result- ";
	builderProduct = builder->getProduct();
	builderProduct->listParts();
	//Prototype example
	std::cout << "------------------------------------------------------\n\nPrototype:\n";
	ConcretePrototype1 prototype("Petko's protytpe", 45.2f);
	prototype.Method(25.3f);
	auto prototype2 = prototype.Clone();
	prototype2->Method(2);
	std::cout << "They have different addresses:" << (&prototype != prototype2.get()) << std::endl;
	//Singleton Example
	std::cout << "------------------------------------------------------\n\nSingleton:\n";
	Singleton& s = Singleton::get();
	std::cout << s.getData() << std::endl;
	s.setData(555);
	std::cout << s.getData() << std::endl;
	//Adapter Example
	std::cout << "------------------------------------------------------\n\nAdapter:\n";
	Adaptee adaptee;
	std::cout << "Adaptee: " << adaptee.SpecificRequest() << std::endl;
	Adapter adapter;
	std::cout << adapter.Request() << std::endl;
	//Bridge Example
	std::cout << "------------------------------------------------------\n\nBridge:\n";
	std::shared_ptr<Implementation> implementationA(new ConcreteImplementationA());
	std::shared_ptr<Implementation> implementationB(new ConcreteImplementationB());
	std::shared_ptr<Abstraction> abstraction(new Abstraction(*implementationA.get()));
	std::shared_ptr<Abstraction> extendedAbstraction(new ExtendedAbstraction(*implementationB.get()));
	std::cout << abstraction->Operation() << std::endl;
	std::cout << extendedAbstraction->Operation() << std::endl;
	//Composite Example
	std::cout << "------------------------------------------------------\n\nComposite:\n";
	Component* leaf1 = new Leaf();
	Component* leaf2 = new Leaf();
	Component* c = new Composite();
	c->Add(leaf1);
	c->Add(leaf2);
	std::cout << leaf1->Operation() << '\n' << c->Operation() << std::endl;
	//Decorator Example
	std::cout << "------------------------------------------------------\n\nDecorator:\n";
	std::shared_ptr<CompositeComponent> compositeComp(new ConcreteComponent());
	std::cout << "I've got a simple component:" << compositeComp->Operation() << std::endl;
	std::shared_ptr<CompositeComponent> decorator1(new ConcreteDecoratorA(compositeComp));
	std::shared_ptr<CompositeComponent> decorator2(new ConcreteDecoratorB(decorator1));
	std::cout << "Now I've got a decorated one\n	" << decorator1->Operation() << std::endl;
	std::cout << "Now I've got a decorated another one\n	" << decorator2->Operation() << std::endl;
	//Facade Example
	std::cout << "------------------------------------------------------\n\nFacade:\n";
	std::shared_ptr<Subsystem1> subsystem1(new Subsystem1());
	std::shared_ptr<Subsystem2> subsystem2(new Subsystem2());
	std::shared_ptr<Facade> facade(new Facade(subsystem1, subsystem2));
	std::cout << facade->Operation() << std::endl;
	//Proxy Example
	std::cout << "------------------------------------------------------\n\nProxy:\n";
	std::shared_ptr<RealSubject> subj(new RealSubject());
	std::cout << subj->Request();
	std::shared_ptr<Proxy> proxy(new Proxy(subj));
	std::cout << proxy->Request() << std::endl;
	//Chain of Responsibility Example
	std::cout << "------------------------------------------------------\n\nResponsibility:\n";
	std::shared_ptr<MonkeyHandler> monkey(new MonkeyHandler());
	std::shared_ptr<SquirrelHandler> squirrel(new SquirrelHandler());
	std::shared_ptr<DogHandler> dog(new DogHandler());
	monkey->SetNext(squirrel)->SetNext(dog);
	std::cout << "Chain: Monkey > Squirrel > Dog\n\n";
	ClientCode(*monkey);
	std::cout << "\n\nSubchain: Squirrel > Dog\n\n";
	ClientCode(*squirrel);
	//Command Example
	std::cout << "------------------------------------------------------\n\nCommand:\n";
	std::shared_ptr<Invoker> invoker(new Invoker());
	std::shared_ptr<Receiver> receiver(new Receiver());
	std::shared_ptr<Command> simpleCommand(new SimpleCommand("Open"));
	std::shared_ptr<Command> complexCommand(new ComplexCommand(receiver, "Generate", "Decode"));
	invoker->SetOnStart(simpleCommand);
	invoker->SetOnFinish(complexCommand);
	invoker->DoSomethingImportant();
	//Mediator Example
	std::cout << "------------------------------------------------------\n\nMediator:\n";
	std::shared_ptr<Component1> component1(new Component1());
	std::shared_ptr<Component2> component2(new Component2());
	std::shared_ptr<ConcreteMediator> mediator(new ConcreteMediator(component1, component2));
	std::cout << "Client triggers operation A.\n";
	component1->DoA();
	std::cout << "\n";
	std::cout << "Client triggers operation D.\n";
	component2->DoD();
	//Memento Example
	std::cout << "------------------------------------------------------\n\nMemento:\n";
	std::srand(static_cast<unsigned int>(std::time(0)));
	std::unique_ptr<Originator> originator(new Originator("Super-duper-super-duper-super."));
	std::unique_ptr<Caretaker> caretaker(new Caretaker(*originator));
	caretaker->Backup();
	originator->DoSomething();
	caretaker->Backup();
	originator->DoSomething();
	caretaker->Backup();
	originator->DoSomething();
	std::cout << "\n";
	caretaker->ShowHistory();
	std::cout << "\nClient: Now, let's rollback!\n\n";
	caretaker->Undo();
	std::cout << "\nClient: Once more!\n\n";
	caretaker->Undo();
	//Observer Example
	std::cout << "------------------------------------------------------\n\nObserver:\n";
	std::shared_ptr<ObserverSubject> subject(new ObserverSubject());
	std::shared_ptr<Observer> observer1(new Observer(subject));
	std::shared_ptr<Observer> observer2(new Observer(subject));
	std::shared_ptr<Observer> observer3(new Observer(subject));
	std::shared_ptr<Observer> observer4;
	std::shared_ptr<Observer> observer5;

	subject->CreateMessage("Hello World! :D");
	observer3->RemoveMeFromTheList();

	subject->CreateMessage("The weather is hot today! :p");
	observer4.reset(new Observer(subject));

	observer2->RemoveMeFromTheList();
	observer5.reset(new Observer(subject));

	subject->CreateMessage("My new car is great! ;)");
	observer5->RemoveMeFromTheList();

	observer4->RemoveMeFromTheList();
	observer1->RemoveMeFromTheList();
	//State Example
	std::cout << "------------------------------------------------------\n\nState:\n";
	std::unique_ptr<Context> context(new Context(std::shared_ptr<State>(new ConcreteStateA())));
	context->Request1();
	context->Request2();

	//Strategy Example
	std::cout << "------------------------------------------------------\n\nStrategy:\n";
	std::unique_ptr<ContextStrategy> contextStrategy(new ContextStrategy(std::shared_ptr<Strategy>(new ConcreteStrategyA)));
	std::cout << "Client: Strategy is set to normal sorting.\n";
	contextStrategy->DoSomeBusinessLogic();
	std::cout << "\n";
	std::cout << "Client: Strategy is set to reverse sorting.\n";
	contextStrategy->set_strategy(std::shared_ptr<Strategy>(new ConcreteStrategyB));
	contextStrategy->DoSomeBusinessLogic();

	//Template Example
	std::cout << "------------------------------------------------------\n\nTemplate:\n";
	std::unique_ptr<AbstractClass> concreteClass1(new ConcreteClass1());
	concreteClass1->TemplateMethod();
	std::cout << "\n";
	std::cout << "Same client code can work with different subclasses:\n";
	std::unique_ptr<AbstractClass> concreteClass2(new ConcreteClass2());
	concreteClass2->TemplateMethod();

	//Visitor Example
	std::cout << "------------------------------------------------------\n\nVisitor:\n";
	std::array<std::shared_ptr<VisitorComponent>, 2> components = { std::shared_ptr<VisitorComponent>(new ConcreteComponentA()),  std::shared_ptr<VisitorComponent>(new ConcreteComponentB()) };
	std::cout << "The client code works with all visitors via the base Visitor interface:\n";
	std::shared_ptr<Visitor> visitor1(new ConcreteVisitor1());
	for (const auto& comp : components) {
		comp->Accept(*visitor1);
	}
	std::cout << "\nIt allows the same client code to work with different types of visitors:\n";
	std::shared_ptr<Visitor> visitor2(new ConcreteVisitor2());
	for (const auto& comp : components) {
		comp->Accept(*visitor2);
	}

	std::cout << "\n\nDestructor from various objects:" << std::endl;
	return 0;
}