#pragma once
#include<memory>
#include<string>
class Context;

class State {
protected:
	Context* context;

public:
	virtual ~State() = default;

	void set_context(Context* new_context) {
		context = new_context;
	}

	virtual void Handle1() = 0;
	virtual void Handle2() = 0;
};

class Context {
private:
	std::shared_ptr<State> state;

public:
	Context(std::shared_ptr<State> state) : state(nullptr) {
		this->TransitionTo(state);
	}

	void TransitionTo(std::shared_ptr<State> state) {
		std::cout << "Context: Transition to " << typeid(*state).name() << ".\n";
		this->state = state;
		this->state->set_context(this);
	}

	void Request1() {
		this->state->Handle1();
	}
	void Request2() {
		this->state->Handle2();
	}
};


class ConcreteStateA : public State {
public:
	void Handle1() override;

	void Handle2() override {
		std::cout << "ConcreteStateA handles request2.\n";
	}
};

class ConcreteStateB : public State {
public:
	void Handle1() override {
		std::cout << "ConcreteStateB handles request1.\n";
	}

	void Handle2() override {
		std::cout << "ConcreteStateB handles request2.\n";
		std::cout << "ConcreteStateB wants to change the state of the context.\n";
		context->TransitionTo(std::shared_ptr<State>(new ConcreteStateA()));
	}
};

void ConcreteStateA::Handle1() {
	std::cout << "ConcreteStateA handles request1.\n";
	std::cout << "ConcreteStateA wants to change the state of the context.\n";
	context->TransitionTo(std::shared_ptr<State>(new ConcreteStateA()));
}