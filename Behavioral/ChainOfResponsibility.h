#pragma once
#include<memory>
#include<string>
#include<vector>
#include<iostream>

class Handler {
public:
	virtual ~Handler() = default;
	virtual std::shared_ptr<Handler> SetNext(std::shared_ptr<Handler> handler) = 0;
	virtual std::string Handle(const std::string& request) = 0;
};

class AbstractHandler : public Handler {
private:
	std::shared_ptr<Handler> next_handler;
public:
	AbstractHandler() : next_handler(nullptr) {}

	virtual std::shared_ptr<Handler> SetNext(std::shared_ptr<Handler> handler) override {
		next_handler = handler;
		return handler;
	}

	virtual std::string Handle(const std::string& request) override {
		return next_handler ? next_handler->Handle(request) : "";
	}
};

class MonkeyHandler : public AbstractHandler {
public:
	virtual std::string Handle(const std::string& request) override {
		if (request == "Banana") {
			return "Monkey: I'll eat the " + request + ".\n";
		}
		else {
			return AbstractHandler::Handle(request);
		}
	}
};

class SquirrelHandler : public AbstractHandler {
public:
	virtual std::string Handle(const std::string& request) override {
		if (request == "Nut") {
			return "Squirrel: I'll eat the " + request + ".\n";
		}
		else {
			return AbstractHandler::Handle(request);
		}
	}
};

class DogHandler : public AbstractHandler {
public:
	virtual std::string Handle(const std::string& request) override {
		if (request == "MeatBall") {
			return "Dog: I'll eat the " + request + ".\n";
		}
		else {
			return AbstractHandler::Handle(request);
		}
	}
};

void ClientCode(Handler& handler) {
	std::vector<std::string> foods = { "Nut", "Banana", "Cup of coffee" };

	for (const std::string& f : foods) {
		std::cout << "Client: Who wants a " << f << "?\n";
		const std::string result = handler.Handle(f);
		if (!result.empty()) {
			std::cout << "  " << result;
		}
		else {
			std::cout << "  " << f << " was left untouched.\n";
		}
	}
}