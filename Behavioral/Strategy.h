#pragma once
#include<string>
#include<memory>
#include<iostream>

class Strategy {
public:
	virtual ~Strategy() = default;
	virtual std::string DoAlgorithm(const std::vector<std::string>& data) const = 0;
};

class ContextStrategy {
private:
	std::shared_ptr<Strategy> strategy;
public:
	ContextStrategy(std::shared_ptr<Strategy> strategy) : strategy(strategy) {}
	ContextStrategy() : strategy(nullptr) {}


	void set_strategy(std::shared_ptr<Strategy> strategy) {
		this->strategy = strategy;
	}

	void DoSomeBusinessLogic() const {
		std::cout << "Context: Sorting data using the strategy (not sure how it'll do it)\n";
		std::string result = strategy->DoAlgorithm(std::vector<std::string>{"a", "e", "c", "b", "d"});
		std::cout << result << "\n";
	}
};

class ConcreteStrategyA : public Strategy {
public:
	virtual std::string DoAlgorithm(const std::vector<std::string>& data) const override {
		std::string result;
		std::for_each(std::begin(data), std::end(data), [&result](const std::string& letter) {
			result += letter;
			});
		std::sort(std::begin(result), std::end(result));

		return result;
	}
};

class ConcreteStrategyB : public Strategy {
	virtual std::string DoAlgorithm(const std::vector<std::string>& data) const override {
		std::string result;
		std::for_each(std::begin(data), std::end(data), [&result](const std::string& letter) {
			result += letter;
			});
		std::sort(std::begin(result), std::end(result));
		for (int i = 0; i < result.size() / 2; i++) {
			std::swap(result[i], result[result.size() - i - 1]);
		}

		return result;
	}
};