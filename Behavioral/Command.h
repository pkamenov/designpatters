#pragma once
#include<string>
#include<memory>
#include<iostream>

class Command {
public:
	virtual ~Command() = default;
	virtual void Execute() const = 0;
};

class SimpleCommand : public Command {
private:
	std::string pay_load;
public:
	explicit SimpleCommand(const std::string& pay_load) : pay_load(pay_load) {}
	virtual void Execute() const override {
		std::cout << "SimpleCommand: See, I can do simple things like printing (" << pay_load << ")\n";
	}
};

class Receiver {
public:
	void DoSomething(const std::string& a) {
		std::cout << "Receiver: Working on (" << a << ".)\n";
	}

	void DoSomethingElse(const std::string& b) {
		std::cout << "Receiver: Also working on (" << b << ".)\n";
	}
};

class ComplexCommand : public Command {
private:
	std::shared_ptr<Receiver> receiver;
	std::string a;
	std::string b;
public:
	ComplexCommand(std::shared_ptr<Receiver> receiver, const std::string& a, const std::string& b) : receiver(receiver), a(a), b(b) {}

	virtual void Execute() const override {
		std::cout << "ComplexCommand: Complex stuff should be done by a receiver object.\n";
		receiver->DoSomething(a);
		receiver->DoSomethingElse(b);
	}
};

class Invoker {
private:
	std::shared_ptr<Command> on_start;
	std::shared_ptr<Command> on_finish;
public:
	void SetOnStart(std::shared_ptr<Command> command) {
		on_start = command;
	}
	void SetOnFinish(std::shared_ptr<Command> command) {
		on_finish = command;
	}

	void DoSomethingImportant() {
		std::cout << "Invoker: Does anybody want something done before I begin?\n";
		if (on_start) {
			on_start->Execute();
		}
		std::cout << "Invoker: ...doing something really important...\n";
		std::cout << "Invoker: Does anybody want something done after I finish?\n";
		if (on_finish) {
			on_finish->Execute();
		}
	}
};