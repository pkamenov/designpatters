#pragma once
#include<memory>
#include<string>
#include<iostream>

class BaseComponent;

class Mediator {
public:
	virtual ~Mediator() = default;
	virtual void Notify(BaseComponent& sender, const std::string& event) const = 0;
};

class BaseComponent {
protected:
	Mediator* mediator;
public:
	BaseComponent(Mediator* mediator) : mediator(mediator) {}
	BaseComponent() : mediator(nullptr) {}

	void set_mediator(Mediator* newMediator) {
		mediator = newMediator;
	}
};

class Component1 : public BaseComponent {
public:
	void DoA() {
		std::cout << "Component 1 does A.\n";
		mediator->Notify(*this, "A");
	}

	void DoB() {
		std::cout << "Component 1 does B.\n";
		mediator->Notify(*this, "B");
	}
};

class Component2 : public BaseComponent {
public:
	void DoC() {
		std::cout << "Component 1 does C.\n";
		mediator->Notify(*this, "C");
	}

	void DoD() {
		std::cout << "Component 1 does D.\n";
		mediator->Notify(*this, "D");
	}
};

class ConcreteMediator : public Mediator {
private:
	Component1& component1;
	Component2& component2;

public:
	ConcreteMediator(std::shared_ptr<Component1>& c1, std::shared_ptr<Component2>& c2) : component1(*c1), component2(*c2) {
		component1.set_mediator(this);
		component2.set_mediator(this);
	}

	virtual void Notify(BaseComponent& sender, const std::string& event) const override {
		if (event == "A") {
			std::cout << "Mediator reacts on A and triggers following operations:\n";
			component2.DoC();
		}
		if (event == "D") {
			std::cout << "Mediator reacts on D and triggers following operations:\n";
			component1.DoB();
			component2.DoC();
		}
	}
};