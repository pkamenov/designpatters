#pragma once
#include<memory>
#include<string>
#include<iostream>

class IObserver {
public:
	virtual ~IObserver() = default;
	virtual void Update(const std::string& message_from_subject) = 0;
};

class ISubject {
public:
	virtual ~ISubject() = default;
	virtual void Attach(IObserver* observer) = 0;
	virtual void Detach(IObserver* observer) = 0;
	virtual void Notify() = 0;
};

class ObserverSubject : public ISubject {
public:
	virtual ~ObserverSubject() {
		std::cout << "Goodbye, I was the Subject.\n";
	}

	virtual void Attach(IObserver* observer) override {
		list_observer.push_back(observer);
	}

	virtual void Detach(IObserver* observer) override {
		list_observer.remove(observer);
	}

	virtual void Notify() override {
		auto iterator = list_observer.begin();
		HowManyObserver();
		while (iterator != list_observer.end()) {
			(*iterator)->Update(message);
			++iterator;
		}
	}

	void CreateMessage(std::string new_message) {
		message = new_message;
		Notify();
	}

	void HowManyObserver() {
		std::cout << "There are " << list_observer.size() << " observers in the list.\n";
	}


	void SomeBusinessLogic() {
		message = "change message";
		Notify();
		std::cout << "I'm about to do some thing important\n";
	}

private:
	std::list<IObserver*> list_observer;
	std::string message;
};

class Observer : public IObserver {
public:
	Observer(std::shared_ptr<ObserverSubject> subject) : subject(subject) {
		subject->Attach(this);
		std::cout << "Hi, I'm the Observer \"" << ++Observer::static_number << "\".\n";
		number = Observer::static_number;
	}
	virtual ~Observer() {
		std::cout << "Goodbye, I was the Observer \"" << number << "\".\n";
	}

	virtual void Update(const std::string& new_message_from_subject) override {
		message_from_subject = new_message_from_subject;
		PrintInfo();
	}

	void RemoveMeFromTheList() {
		subject->Detach(this);
		std::cout << "Observer \"" << number << "\" removed from the list.\n";
	}

	void PrintInfo() {
		std::cout << "Observer \"" << number << "\": a new message is available --> " << message_from_subject << "\n";
	}

private:
	std::string message_from_subject;
	std::shared_ptr<ObserverSubject> subject;
	static int static_number;
	int number;
};

int Observer::static_number = 0;