#pragma once
#include<memory>
#include<string>
#include<iostream>
#include<ctime>
#include<random>
#include<iostream>

class Memento {
public:
	virtual ~Memento() = default;
	virtual std::string GetName() const = 0;
	virtual std::string get_date() const = 0;
	virtual std::string get_state() const = 0;
};

class ConcreteMemento : public Memento {
private:
	std::string state;
	std::string date;
public:
	ConcreteMemento(const std::string& state) : state(state) {
		std::time_t now = std::time(0);
		date = std::ctime(&now);
	}

	virtual std::string get_state() const override {
		return state;
	}

	virtual std::string GetName() const override {
		return date + " / (" + state.substr(0, 9) + "...)";
	}

	virtual std::string get_date() const override {
		return date;
	}
};

class Originator {
private:
	std::string state;

	std::string GenerateRandomString(int length = 10) {
		const char alphanum[] =
			"0123456789"
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			"abcdefghijklmnopqrstuvwxyz";

		std::size_t stringLength = sizeof(alphanum) - 1;

		std::string random_string;
		for (int i = 0; i < length; i++) {
			random_string += alphanum[std::rand() % stringLength];
		}
		return random_string;
	}

public:
	Originator(const std::string& state) : state(state) {
		std::cout << "Originator: My initial state is: " << state << "\n";
	}

	void DoSomething() {
		std::cout << "Originator: I'm doing something important.\n";
		state = GenerateRandomString(30);
		std::cout << "Originator: and my state has changed to: " << state << "\n";
	}

	std::shared_ptr<Memento> Save() {
		return std::shared_ptr<Memento>(new ConcreteMemento(state));
	}

	void Restore(std::shared_ptr<Memento> memento) {
		state = memento->get_state();
		std::cout << "Originator: My state has changed to: " << state << "\n";
	}
};

class Caretaker {
private:
	std::vector<std::shared_ptr<Memento>> mementos;

	Originator& originator;

public:
	Caretaker(Originator& originator) : originator(originator) {}

	void Backup() {
		std::cout << "\nCaretaker: Saving Originator's state...\n";
		mementos.push_back(originator.Save());
	}

	void Undo() {
		if (!this->mementos.size()) {
			return;
		}

		std::shared_ptr<Memento> memento = mementos.back();
		mementos.pop_back();
		std::cout << "Caretaker: Restoring state to: " << memento->GetName() << "\n";
		try {
			originator.Restore(memento);
		}
		catch (...) {
			Undo();
		}
	}

	void ShowHistory() const {
		std::cout << "Caretaker: Here's the list of mementos:\n";
		for (const auto& memento : mementos) {
			std::cout << memento->GetName() << "\n";
		}
	}
};