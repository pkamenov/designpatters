#pragma once
#include<string>
#include<iostream>

class ConcreteComponentA;
class ConcreteComponentB;

class Visitor {
public:
	virtual ~Visitor() = default;
	virtual void VisitConcreteComponentA(const ConcreteComponentA& element) const = 0;
	virtual void VisitConcreteComponentB(const ConcreteComponentB& element) const = 0;
};

class VisitorComponent {
public:
	virtual ~VisitorComponent() = default;
	virtual void Accept(const Visitor& visitor) const = 0;
};

class ConcreteComponentA : public VisitorComponent {
public:
	virtual void Accept(const Visitor& visitor) const override {
		visitor.VisitConcreteComponentA(*this);
	}

	std::string ExclusiveMethodOfConcreteComponentA() const {
		return "A";
	}
};

class ConcreteComponentB : public VisitorComponent {
public:
	virtual void Accept(const Visitor& visitor) const override {
		visitor.VisitConcreteComponentB(*this);
	}

	std::string SpecialMethodOfConcreteComponentB() const {
		return "B";
	}
};


class ConcreteVisitor1 : public Visitor {
public:
	virtual void VisitConcreteComponentA(const ConcreteComponentA& element) const override {
		std::cout << element.ExclusiveMethodOfConcreteComponentA() << " + ConcreteVisitor1\n";
	}

	virtual void VisitConcreteComponentB(const ConcreteComponentB& element) const override {
		std::cout << element.SpecialMethodOfConcreteComponentB() << " + ConcreteVisitor1\n";
	}
};

class ConcreteVisitor2 : public Visitor {
public:
	virtual void VisitConcreteComponentA(const ConcreteComponentA& element) const override {
		std::cout << element.ExclusiveMethodOfConcreteComponentA() << " + ConcreteVisitor2\n";
	}
	void VisitConcreteComponentB(const ConcreteComponentB& element) const override {
		std::cout << element.SpecialMethodOfConcreteComponentB() << " + ConcreteVisitor2\n";
	}
};