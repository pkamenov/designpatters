#pragma once
#include<string>
#include<memory>

class AbstractProductA {
public:
	virtual ~AbstractProductA() = default;
	virtual std::string operator()() const = 0;
};

class ConcreteProductA1 : public AbstractProductA {
public:
	virtual std::string operator()() const override {
		return "Calling operator of ConcreteProduct A1";
	}
};

class ConcreteProductA2 : public AbstractProductA {
public:
	virtual std::string operator()() const override {
		return "Calling operator of ConcreteProduct A2";
	}
};

class AbstractProductB {
public:
	virtual ~AbstractProductB() = default;
	virtual std::string operator()() const = 0;
};

class ConcreteProductB1 : public AbstractProductB {
public:
	virtual std::string operator()() const override {
		return "Calling operator of ConcreteProduct B1";
	}
};

class ConcreteProductB2 : public AbstractProductB {
public:
	virtual std::string operator()() const override {
		return "Calling operator of ConcreteProduct B2";
	}
};

class AbstractFactory {
public:
	virtual ~AbstractFactory() = default;
	virtual std::shared_ptr<AbstractProductA> createProductA() const = 0;
	virtual std::shared_ptr<AbstractProductB> createProductB() const = 0;
};

class ConcreteFactory1 : public AbstractFactory {
public:
	virtual std::shared_ptr<AbstractProductA> createProductA() const override {
		return std::make_shared<ConcreteProductA1>();
	}
	virtual std::shared_ptr<AbstractProductB> createProductB() const override {
		return std::make_shared<ConcreteProductB1>();
	}
};

class ConcreteFactory2 : public AbstractFactory {
public:
	virtual std::shared_ptr<AbstractProductA> createProductA() const override {
		return std::make_shared<ConcreteProductA2>();
	}
	virtual std::shared_ptr<AbstractProductB> createProductB() const override {
		return std::make_shared<ConcreteProductB2>();
	}
};