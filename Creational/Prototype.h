#pragma once
#include<memory>
#include<string>

class Prototype {
protected:
	std::string prototype_name;
	float prototype_field;

public:
	Prototype() = default;
	Prototype(const std::string& prototype_name)
		: prototype_name(prototype_name) {}
	virtual ~Prototype() = default;
	virtual std::shared_ptr<Prototype> Clone() const = 0;
	virtual void Method(float prototype_field) {
		prototype_field = prototype_field;
		std::cout << "Call Method from " << prototype_name << " with field : " << prototype_field << std::endl;
	}
};

class ConcretePrototype1 : public Prototype {
private:
	float concrete_prototype_field1_;

public:
	ConcretePrototype1(const std::string& prototype_name, float concrete_prototype_field)
		: Prototype(prototype_name), concrete_prototype_field1_(concrete_prototype_field) {}

	virtual std::shared_ptr<Prototype> Clone() const override {
		return std::make_unique<ConcretePrototype1>(*this);
	}
};

class ConcretePrototype2 : public Prototype {
private:
	float concrete_prototype_field2;

public:
	ConcretePrototype2(const std::string& prototype_name, float concrete_prototype_field)
		: Prototype(prototype_name), concrete_prototype_field2(concrete_prototype_field) {}

	virtual std::shared_ptr<Prototype> Clone() const override {
		return std::make_unique<ConcretePrototype2>(*this);
	}
};