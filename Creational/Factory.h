#pragma once

#include<string>
#include<memory>
class Product {
public:
	virtual ~Product() = default;
	virtual std::string operator()() const = 0;
};

class ConcreteProduct1 : public Product {
public:
	virtual std::string operator()() const override {
		return "Calling operator of ConcreteProduct 1";
	}
};

class ConcreteProduct2 : public Product {
public:
	virtual std::string operator()() const override {
		return "Calling operator of ConcreteProduct 2";
	}
};

class Creator {
public:
	virtual ~Creator() = default;
	virtual std::shared_ptr<Product> FactoryMethod() const = 0;

	std::string operator()() const {
		std::shared_ptr<Product> product = this->FactoryMethod();
		std::string result = "Creator: The same creator's code has just worked with " + (*product)();
		return result;
	}
};

class ConcreteCreator1 : public Creator {
public:
	virtual std::shared_ptr<Product> FactoryMethod() const override {
		return std::make_shared<ConcreteProduct1>();
	}
};

class ConcreteCreator2 : public Creator {
public:
	virtual std::shared_ptr<Product> FactoryMethod() const override {
		return std::make_shared<ConcreteProduct2>();
	}
};