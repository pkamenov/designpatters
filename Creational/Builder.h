#pragma once
#include<vector>
#include<string>
#include<memory>
class Product1 {
public:
	std::vector<std::string> parts;
	void listParts() const {
		std::cout << "Product parts: ";
		for (size_t i = 0; i < parts.size(); i++) {
			if (parts[i] == parts.back()) {
				std::cout << parts[i];
			}
			else {
				std::cout << parts[i] << ", ";
			}
		}
		std::cout << "\n\n";
	}
};

class Builder {
public:
	virtual ~Builder() = default;
	virtual void ProducePartA() const = 0;
	virtual void ProducePartB() const = 0;
	virtual void ProducePartC() const = 0;
};

class ConcreteBuilder1 : public Builder {
private:
	std::shared_ptr<Product1> product;
public:
	ConcreteBuilder1() {
		this->Reset();
	}

	void Reset() {
		product = std::make_shared<Product1>();
	}
	virtual void ProducePartA() const override {
		product->parts.push_back("PartA1");
	}

	virtual void ProducePartB()const override {
		product->parts.push_back("PartB1");
	}

	virtual void ProducePartC()const override {
		product->parts.push_back("PartC1");
	}

	std::shared_ptr<Product1> getProduct() {
		auto result = product;
		Reset();
		return result;
	}

	/**
	 * Concrete Builders are supposed to provide their own methods for
	 * retrieving results. That's because various types of builders may create
	 * entirely different products that don't follow the same interface.
	 * Therefore, such methods cannot be declared in the base Builder interface
	 * (at least in a statically typed programming language).
	 *
	 * Usually, after returning the end result to the client, a builder instance
	 * is expected to be ready to start producing another product. That's why
	 * it's a usual practice to call the reset method at the end of the
	 * `getProduct` method body. However, this behavior is not mandatory, and
	 * you can make your builders wait for an explicit reset call from the
	 * client code before disposing of the previous result.
	 */

};

class Director {
private:
	Builder& builder;
public:
	Director(Builder& builder) : builder(builder) {}

	void BuildMinimalViableProduct() {
		builder.ProducePartA();
	}

	void BuildFullFeaturedProduct() {
		builder.ProducePartA();
		builder.ProducePartB();
		builder.ProducePartC();
	}
};