#pragma once
#include<memory>
class Singleton {
public:
	Singleton(const Singleton&) = delete;
	Singleton& operator=(const Singleton&) = delete;

	static Singleton& get() {
		static Singleton instance;
		return instance;
	}

	int getData() const {
		return data;
	}

	void setData(int newData) {
		data = newData;
	}

private:
	int data = 0;

	Singleton(int data = 0) : data(data) {}

	static Singleton sInstance;
};